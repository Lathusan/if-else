package com.three;

import java.util.Scanner;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * @Jan 23, 2021
 **/

public class IfElseSample {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.print("Enter your Tamil marks : ");
		double tamil = scan.nextDouble();

		System.out.print("Enter your Maths marks : ");
		double maths = scan.nextDouble();

		System.out.print("Enter your Seience marks : ");
		double science = scan.nextDouble();

		System.out.print("Enter your English marks : ");
		double english = scan.nextDouble();

		System.out.print("Enter your Ict marks : ");
		double ict = scan.nextDouble();

		double total = (tamil + maths + english + ict + science) / 5;

		if (total <= 100 && total >= 75) {
			System.out.println("\nYour avarage grade is : A");

		} else if (total <= 74 && total >= 65) {
			System.out.println("\nYour avarage grade is : B");

		} else if (total <= 64 && total >= 55) {
			System.out.println("\nYour avarage grade is : C");

		} else if (total <= 54 && total >= 35) {
			System.out.println("\nYour avarage grade is : S");

		} else if (total <= 34 && total >= 0) {
			System.out.println("\nYour avarage grade is : W");

		} else {
			System.err.println("\nPlease enter valide marks!!!");
		}

	}

}
